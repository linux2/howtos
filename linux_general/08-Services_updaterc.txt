                                                                                                         ___________
                                                                                                        |           |
========================================================================================================| UPDATE-RC |===============================================================================================================================
                                                                                                        |___________|

 ================
| Comando Ãºtiles |----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ================


What services are available for startup?
----------------------------------------

  >service --status-all


Start a service
---------------

  >service apache2 start


Stop a service
--------------

  >service apache2 stop


Check status of a service
-------------------------

  >service apache2 status


Add a service
-------------

  >update-rc.d apache2 defaults


Remove a service
----------------

  >update-rc.d -f apache2 remove



 ===================================================
| CÃ³mo crear scripts de inicio de forma normalizada |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ===================================================

  1) Creamos un script en /etc/init.d:

      >sudo nano /etc/init.d/prueba.sh

  2) Le aÃ±adimos una cabecera como esta:

 # Author: Etxahun Sanchez Bazterretxea
 #
 ### BEGIN INIT INFO
 # Provides:          ---------
 # Required-Start:    ---------
 # Should-Start:      ---------
 # Required-Stop:     ---------
 # Should-Stop:       ---------
 # Default-Start:     ---------
 # Default-Stop:      ---------
 # Short-Description: <short-description>
 # Description:       <long-description>
 ### END INIT INFO

    Donde los diferentes campos quieren decir:

	- Provides: define los nombres de los servicios que proporciona el script. Suele tratarse de el nombre de un demonio.
	- Required-Start: define quÃ© demonios deben estar disponibles para poder iniciar y arrancar nuestro servicio. El sistema init es el responsable de asegurar que estos servicios estÃ©n arrancados de antemano.
	- Should-Start: es un campo opcional. Su funciÃ³n es similar a Required-Start, pero no conlleva una dependencia estricta. Si un servicio listado aquÃ­ no estÃ¡ instalado, o no se arranca de forma automÃ¡tica, simplemente se ignora.
	- Required-Stop: define los servicios que deben estar todavÃ­a disponibles durante el apagado de nuestro servicio.
	- Should-Stop: es un campo OPCIONAL. Su funciÃ³n es similar a Required-Stop, pero no conlleva una dependencia estricta. Si un servicio de los que se listan no estÃ¡ instalado, o no se arranca de forma automÃ¡tica, simplemente se ignora.
	- Default-Start: define en quÃ© niveles de ejecuciÃ³n deberÃ­a arrancarse nuestro script de forma automÃ¡tica.
	- Default-Stop: define los niveles de ejecuciÃ³n en los que nuestro script deberÃ­a detenerse de forma automÃ¡tica.
	- Short-Description: especifica una descripciÃ³n sencilla de la funciÃ³n del servicio, en una sola lÃ­nea.
	- Description: es una descripciÃ³n mÃ¡s detallada de la funciÃ³n de nuestro servicio. Cada lÃ­nea adicional debe empezar con el sÃ­mbolo '#', seguido por carÃ¡cter de tabulado, o al menos dos espacios. Esta descripciÃ³n multilÃ­nea acaba cuando se encuentra la 
		       primera lÃ­nea que no coincide con este patrÃ³n.


                                                                                                         _________
                                                                                                        |         |
========================================================================================================| SYSTEMD |===============================================================================================================================
                                                                                                        |_________|

He creado un "Systemd Service" de la siguiente manera:

  1) Creamos en la ruta que queramos un fichero "mi_servicio.service":

      >sudo nano mi_servicio.service

[Unit]
Description=Nextel MQTT client
After=network-online.target

[Service]
ExecStart=/home/pirate/mqtt_client_on_C/cliente_mqtt_nextel
Restart=always
RestartSec=10
# Output to syslog
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=nextel-mqtt-client

[Install]
WantedBy=multi-user.target

  2) A continuaciÃ³n lo copiamos a la carpeta de "systemd":

      >sudo cp mi_servicio.service /etc/systemd/system/mi_servicio.service

  3) Podemos empezar a utilizarlo de la siguiente manera:

      >sudo systemctl start mi_servicio.service

  4) Si queremos que se CARGUE AUTOMATICAMENTE EN EL INICIO:

     >sudo systemctl enable mi_servicio.service

