Cuando arranquemos el portÃ¡til en modo CLI, si tenemos instalado "Network Manager", la manera de conectarnos a una Wi-Fi nueva es a travÃ©s de "NMCLI" (Network Manager CLI):

1) Comprobar el estado de las interfaces:

   $ nmcli d
  
  DEVICE             TYPE      STATE         CONNECTION
  ...
  wlan0              wifi      disconnected     --
  
2) Nos aseguramos que el interfaz Wi-Fi estÃ¡ encendido:

   $ nmcli r wifi on
   
3) Obtenemos un listado de las Wi-Fi disponibles:

   $ nmcli d wifi list
   *  SSID           MODE   CHAN  RATE       SIGNAL  BARS  SECURITY
      ...
      my_wifi      Infra  5     54 Mbit/s  89      ââââ  WPA2

4) Nos conectamos a la Wi-Fi que queramos:

   $ nmcli d wifi connect <my_wifi> password <password>

