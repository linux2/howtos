# SUBSCRIBE to topic "hello/world":

  >mosquitto_sub -d -t hello/world


# PUBLISH to topic "hello/world":

  >mosquitto_pub -d -t hello/world -m "Hello from Terminal window 2!"

