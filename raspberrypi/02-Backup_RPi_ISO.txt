It may be sensible for you to keep a copy of the entire SD card image, so you can restore the whole SD card if you lose it or it becomes corrupt. You can do this using the same method 
you'd use to write an image to a new card, but in reverse.

0) LISTAMOS LOS DICOS:
----------------------

 >fdisk -l
 

A) CREAR IMAGEN (Sin CompresiÃ³n):
---------------------------------
This will create an image file on your PC which you can use to write to another SD card, and keep exactly the same contents and settings. 

 >sudo dd bs=4M if=/dev/mmcblk0 of=raspbian.img
 

B) CREAR IMAGEN (Con CompresiÃ³n):
---------------------------------
These files can be very large, and compress well. To compress, you can pipe the output of dd to gzip as well to get a compressed file that is significantly smaller than the original size:

 >sudo dd bs=4M if=/dev/mmcblk0 | gzip > raspbian.img.gz


C) RESTAURAR (Sin CompresiÃ³n):
-----------------------------
To restore or clone to another card, use dd in reverse:

 >sudo dd bs=4M if=raspbian.img of=/dev/mmcblk0
  

D) RESTAURAR (Con CompresiÃ³n):
-----------------------------
To restore, pipe the output of gunzip to dd:

 >gunzip --stdout rasbian.img.gz | sudo dd bs=4M of=/dev/mmcblk0
