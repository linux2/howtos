Referencias:
------------

 - http://www.howtogeek.com/135533/how-to-use-rsync-to-backup-your-data-on-linux/
 - https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories-on-a-vps


Sincronizar el contenido de SOURCE y DESTINATION folder
=======================================================

>rsynz -azP <source_path>/ <destination_path>

  -a: It stands for "archive" and syncs recursively and preserves symbolic links, special and device files, modification times, group, owner, and permissions.
  -z: Add compression. 
  -P: combines the flags --progress and --partial. The first of these gives you a progress bar for the transfers and the second allows you to resume interrupted transfers.
  
  Nota: Es importante incluir el "/" final ya que sino, nos crearÃ­a un directorio del mismo nombre que <sourcepath> en el directorio de destino <destination_path>



Backups incrementales con RSYNC y RPi por SSH
=============================================

Cosas que necesitamos tener instaladas previamente:

 >sudo apt-get install sshpass
 
Esto nos va a permitir enviar por lÃ­nea de comandos la contraseÃ±a de conexiÃ³n a nuestro servidor remoto ssh.

El comando para realizar el backup con RSYNC serÃ­a el siguiente:

 >rsync -ogpEav --compress-level=9 -e 'sshpass -p password ssh -o StrictHostKeyChecking=no -p 1234' root@1.2.3.4:/var/www/miweb/* /var/hdd/miweb/
 
	ExplicaciÃ³n:
	
	    a) rsync: programa que se encarga de realizar todo el proceso.
	    b) -ogpEav: esta lÃ­nea realizarÃ¡ las siguientes acciones:
	    
	                 o: mantiene los propietarios de los archivos.
	                 g: lo mismo pero mantienen los grupos.
	                 p: mantiene permisos de archivos.
	                 E: si un archivo es ejecutable sigue siÃ©ndolo.
	                 a: transferencia en modo archivo.
	                 v: verbose, es decir, muestra la salida de lo que estÃ¡ haciendo rsync (nos vale para saber si lo hace todo correcto)
	                 
	    c) --compress-level=9 : con esta parte hacemos que se compriman los archivos con la tasa mÃ¡xima antes de ser descargados, gasta mÃ¡s cpu, pero si tenemos una conexiÃ³n lenta 
	                            nos va a ir de perlas.	                            
	    d) -e: nos va a permitir ejecutar un comando en esta lÃ­nea.
	    e) âsshpass -p PASSWROD ssh -o StrictHostKeyChecking=no -p 1234â: AquÃ­ ejecutamos el comando "sshpass" y le indicamos que el password (-p) es PASSWORD (pon el tuyo para el 
	                                                                      usuario que vayas a usar). 
	                                                                      
	                                                                      -o: nos permite aÃ±adir opciones y "StrictHostKeyChecking=no" nos evitarÃ¡ problemas con las key ssh que no 
	                                                                          vamos a usar. 
	                                                                      -p 1234: nos vale para indicar el puerto por el que conectar al ssh. Podemos suprimir las partes del puerto 
	                                                                               u opciones si no nos van a dar problemas, el puerto predeterminado de ssh sabemos que es el 22.

	    f)root@1.2.3.4:/var/www/miweb/*: carpeta de origen de la que haremos la copia, que al ser por ssh se compone de:
	    
	                                        -root: usuario con el que nos conectaremos.
	                                        -@1.2.3.4: IP en la que estÃ¡ el equipo remoto (da igual por lan o por Internet).
	                                        -/var/www/miweb/*: ruta remota a la carpeta que vamos a copiar.
	                                        
	    g) /var/hdd/miweb/: Por Ãºltimo ya solo nos queda esta ruta local de la carpeta destino donde se guardarÃ¡ la copia completa de los archivos remotos.
	    
	    
	    
	    
	    
	    
	    
