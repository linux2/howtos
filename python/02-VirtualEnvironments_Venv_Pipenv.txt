#########################################################################################################################################
#                                                                                                                                       #
# Referencias:                                                                                                                          #
#                                                                                                                                       #
#    Pipenv:                                                                                                                            #
#                                                                                                                                       #
#    - How to manage your Python projects with Pipenv: https://robots.thoughtbot.com/how-to-manage-your-python-projects-with-pipenv     #
#    - Getting Started with Pipenv: http://polyglot.ninja/pipenv-getting-started/                                                       #
#                                                                                                                                       #
#    Virtualenv:                                                                                                                        #
#                                                                                                                                       #
#    - https://realpython.com/python-virtual-environments-a-primer/                                                                     #
#    - http://docs.python-guide.org/en/latest/dev/virtualenvs/                                                                          #
#                                                                                                                                       #
#########################################################################################################################################

####################################################### IMPORTANTE ######################################################################
#                                                                                                                                       #
#  NOTA: Con Python3 ya esta incluido el soporte para la creaciÃ³n de "virtual environments":                                            #
#  ----                                                                                                                                 #
#          $ python3 -m venv <nombre_del_venv>                                                                                          #
#                                                                                                                                       #
#        Una vez creado, entramos el directorio y activamos el virtenv que hemos creado:                                                # 
#                                                                                                                                       #
#          $cd <nombre_del_venv>                                                                                                        #
#          $<nombre_del_venv>/source env/bin/activate                                                                                   #
#                                                                                                                                       #
#        Una vez estÃ© activado, veremos como nos pone un (env) por delante del path.                                                    #
#        Para desactivarlo, tan sÃ³lo tendremos que escribir lo siguiente:                                                               #
#                                                                                                                                       #
#          $(env)deactivate                                                                                                             #
#                                                                                                                                       #
########################################################################################################################################

A la hora de trabajar con "Virtual Environments" en Python, tenemos varias opciones:

  A) Virtualenv (tanto en Python 2.x como en Python 3.x) 
  B) Pipenv (desarrollado por Kenneth Reitz)

#################
#               #
# A) VIRTUALENV ########################################################################################################################
#               #
#################

 Prerequisitos
 -------------

 1) InstalaciÃ³n de "PiP":

      $ sudo apt-get install python-pip

 2) InstalaciÃ³n de "virtualenv":

      $ pip install virtualenv


 Nuevo "Virtual Environment"
 ---------------------------

 1) Creamos un directorio:

      $ mkdir proyecto_lana

 2) Entramos dentro del directorio y creamos el nuevo "virtual environment" denominado "primer_proyecto":

      $ cd proyecto_lana

    En Python 2:

      $ virtualenv primer_proyecto

    En Python 3:
   
      $ python3 -m venv primer_proyecto

 3) Pero antes de hacer nada, estando dentro del directorio âproyectos_lanaâ tendremos que ACTIVAR el âvirtual environmentâ: 

      $ source primer_proy/bin/activate

 4) Tras hacer el paso anterior veremos cÃ³mo al inicio del âpromptâ del sistema aparecerÃ¡ â(primer_proy)â:  

      $ (primer_proy)root@idipi:/home/esb

    A partir de este momento, todo lo que instalemos, afectarÃ¡ Ãºnicamente a (primer_proy), estemos o no en el directorio âproyectos_lana/primer_proyâ:

 5) Crearemos nuestro programas Python dentro del directorio â/proyectos_lana/primer_proy/â de manera normal (creando la estructura de carpetas que nos de la gana).

 6) Por Ãºltimo, cuando queramos terminar/salir del âvirtual environmentâ tendremos que desactivar el âvirtualenvâ:  

    $ deactivate


 NOTA: En el versiÃ³n de Ubuntu que tengo instalada no se produce el cambio en el prompt cuando hacemos el "source bin/activate". Para saber si lo tenemos activado haremos lo siguiente:
 ----
         $ pipenv shell -c

       Y en caso de encontrarse activado nos indicarÃ¡ en que path estÃ¡ activado. Ejemplo:

       "Shell for /home/esb/03-proyectos/<proyecto> already activated."


#################
#               #
# B) PIPENV     ########################################################################################################################
#               #
#################

1) InstalaciÃ³n:

     $ pip install pipenv

2) Nos movemos al directorio donde tenemos nuestro proyecto en Python:

     $ cd my_project
     $ pipenv install

   Con "pipenv install" se crean dos nuevos ficheros, "Pipfile" y "Pipfile.lock" dentro del directorio y un nuevo entorno virtual en la siguiente ruta: /home/esb/.local/share/virtualenvs
   Podremos trabajar en Python 2 o 3 dependiendo del flag que le pongamos:

	A) pipenv install --two: Para Python 2.
	B) pipenv install --three: Para Python3.

   - Pipfiles: Contiene informaciÃ³n acerca de las dependencias de nuestro proyecto y sustituye al tradicional "requirements.txt".

3) Instalar paquetes dentro nuestro proyecto "pipenv":

     $ pipenv install beautifulsoup4

   Si quisiÃ©ramos desinstalarlo, tan sÃ³lo tenemos que hacer:

     $ pipenv uninstall beautifulsoup4

   Si queremos bloquear (FROZEN) nuestra lista de paquetes/dependencias que hayamos instalado, lo haremos con el siguiente comando:

     $ pipenv lock

   NOTA: Es recomendable incluir en GIT el fichero "pipfiles" para que otro usuario que clone nuestro proyecto pueda instalar todas las dependencias con el comando:

     $ pipenv install

4) Arrancamos nuestro proyecto con:

     $ pipenv run python my_proyect.py

   NOTA: Podemos utilizar el siguiente alias para que podamos hacerlo mÃ¡s rÃ¡pido:

         alias prp = 'pipenv run python"


5) Cuando hayamos terminado y queramos borrar el entorno virtual haremos lo siguiente:

     $ exit
     $ pipenv --rm
