#########################################################################################################################################################################################
################################################################################# SCRAPY ################################################################################################
#########################################################################################################################################################################################

 ########################
 # ALMACENAR EN MongoDB #
 ########################

  Referencia:
  
     - https://realpython.com/blog/python/web-scraping-with-scrapy-and-mongodb/


1) Instalar MongoDB

   $ pip install pymongo

2) Almacenar los datos en MongoDB:

   Cada vez que el Spider nos devuelva un item (mediante "yield item") tendremos que validarlo y aÃ±adirlo a una "collection" (tabla) de MongoDB.
   Para ello, lo primero que haremos es crear la base de datos donde almacenaremos la informaciÃ³n "scrapeada" por nuestro spider:
   
   
   2-1) Editamos el fichero "settings.py" y especificamos el "pipeline" donde configuramos los datos de nuestra base de datos:
   
	        ITEM_PIPELINES = ['stack.pipelines.MongoDBPipeline', 300]
        
	        MONGODB_SERVER = "localhost"
	        MONGODB_PORT = 27017
	        MONGODB_DB = "mejortorrent"
	        MONGODB_COLLECTION = "peliculas"
        
        Nota: el nÃºmero 300 puede ser sustituido por un valor entre 0 y 1000. Sirve para aquellos casos en los que tengamos mÃ¡s de una tarea a realizar y queramos secuenciarlas. 
        
   2-2) Hasta el momento, tenemos creado un "spider" que es el que estÃ¡ extrayendo la informaciÃ³n de la pÃ¡gina web y por otra parte hemos configurado (en el fichero anterior 
        "settings.py") los datos de MongoDB. Lo que nos falta es interconectar ambas tareas. Para ello, editaremos el fichero "pipelines.py":
        
        
             Fichero "pipelines.py" original
             -------------------------------
        
              class MejortorrentPipeline(object):
                  def process_item(self, item, spider):
                      return item
                      
        
        
             Fichero "pipelines.py" con los datos relativos a MongoDB
             --------------------------------------------------------
        
               import pymongo
          
               from scrapy.conf import settings
               from scrapy.exceptions import DropItem
          
               class MongoDBPipeline(object):
          
                   def __init__(self):
                           connection = pymongo.MongoClient(
                                                            settings['MONGODB_SERVER'],
                                                            settings['MONGODB_PORT']
                                                           )
                           db = connection[settings['MONGODB_DB']]
                           self.collection = db[settings['MONGODB_COLLECTION']]
                                                                      
               
                   def process_item(self, item, spider):
                       valid = True
                       for data in item:
                           if not data:
                              valid = False
                              raise DropItem("Missing {0}!".format(data))
                       if valid:
                              self.collection.insert(dict(item))
                              spider.log("PelÃ­cula aÃ±adida a la base de datos MongoDB!")
                       return item

	---------------------------------------------------------------------------------------------------------------
	
	ExplicaciÃ³n: 
	
	    - En la funciÃ³n "__init__" configuramos la conexiÃ³n a base de datos. Para ello utilizamos las variables que previamente hemos definido en "settings.py" y que hemos importado
	      mediante "from scrapy.conf import settings".
	      
	    - La funciÃ³n "process_item" recibe el "item" y el "spider" y lo que hace es comprobar si viene vacio o no. Si no viene vacio:
	    
	              self.collection.insert(dict(item))
	              spider.log("PelÃ­cula aÃ±adida a la base de datos MongoDB!")
	              
	      es decir, insertamos los datos en la base ded datos: self.collection.insert(dict(item))
	       

3) Arrancamos Scrapy:

   $ scrapy crawl <nombre_spider>
