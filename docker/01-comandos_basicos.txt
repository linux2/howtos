 ################### 
 # CONTAINERS      #
 ###################
 
 1) Ver containers arrancados:                          >docker ps
 2) Ver "todo el historial" de containers arrancados:   >docker ps -a
 3) Modificar nombre de container:                      >docker rename <CONTAINER_NAME> <CONTAINER_NEW_NAME>

 ################### 
 # IMAGES          #
 ###################

 1) Ver listado de imagenes:                            >docker images
 2) Ver "todo el historial" de imÃ¡genes arrancadas:     >docker images -a

 ################### 
 # CREAR IMAGES    #
 ###################
 
 1) Creamos el fichero "Dockerfile" y ejecutamos lo siguiente:

    >docker build -t <image_name> .

 ################### 
 # CREAR CONTAINER #
 ###################

 1) A partir de una IMAGE, ejecutamos lo siguiente:

   >docker run -ti --name <nombre_que_queramos_dar> <image_name> /bin/bash

 ###################      
 # ACCESS TO SHELL #
 ###################      

 1) Si el container estÃ¡ arrancado con "/bin/bash" podremos conectarnos con ATTACH:

    >docker attach <CONTAINER_NAME/ID>

 2) Si el container NO estÃ¡ arancado con "/bin/bash":

    >sudo docker exec -it <CONTAINER ID/NAME> bash

 ###################      
 # HOUSEKEEPING    #
 ###################      

 A) Borrar container:                                   >docker rm <container_ID>
 B) Borrar TODOS los containers:                        >docker rm $(docker ps -a -q)
 C) Borrar imÃ¡genes:                                    >docker rmi <container_ID>
 D) Borrar TODAS las imÃ¡ganes:                          >docker rmi $(docker images -q)
 E) Borrar "unused images":                             >docker image prune -a


 ###################      
 # DUDAS           #
 ###################      

 A) CMD:
    ---
    The CMD instruction should be used to run the software contained by your image.

    - Ejemplo:

	 FROM ubuntu:15.04
	 COPY . /app
	 RUN make /app
	 CMD python /app/app.py

    CMD specifies what command to run within the container.

 B) EXPOSE:
    ------
    The EXPOSE instruction INDICATES (but not exposes) the ports on which a container listens for connections.
    Para que los puertos quede expuestos hay que indicarlos bien el "docker run -p <PORT>" o en el "docker-compose.yml".


 C) ENV:
    ---
    Para indicarle al contenedor variables de entorno. 

    - Ejemplo:

        FROM alpine
	ENV ADMIN_USER="mark"
	RUN echo $ADMIN_USER > ./mark
	RUN unset ADMIN_USER	
	CMD sh

     Si ejecutamos el Dockerfile, veremos lo siguiente:

      >docker run -rm -it test sh echo $ADMIN_USER
       
       mark

 D) ADD or COPY:
    -----------
    Although ADD and COPY are functionally similar, generally speaking, COPY is preferred. 


 E) ENTRYPOINT:
    ----------
    An ENTRYPOINT allows you to configure a container that will run as an executable.
    ENTRYPOINT has two forms:

	1) ENTRYPOINT ["executable", "param1", "param2"] (exec form, preferred)
	2) ENTRYPOINT command param1 param2 (shell form)
    


 F) CMD vs ENTRYPOINT:
    -----------------
    Docker has a default entrypoint which is "/bin/sh -c" but does not have a default command.
    When you run docker like this: 
   
      >docker run -i -t ubuntu bash 

    The entrypoint is the default "/bin/sh -c", the image is "ubuntu" and the command is "bash".
    
    Everything after "ubuntu" in the example above is the command and is passed to the "entrypoint". When using the "CMD" 
    instruction, it is exactly as if you were doing:

      >docker run -i -t ubuntu <cmd> 

    where <cmd> will be the parameter of the entrypoint.

     - Ejemplo:

	# Dockerfile
	FROM ubuntu
	ENTRYPOINT ["/bin/cat"]

     Si ejeuctamos lo siguiente:

       >docker build -t imagen_prueba .
       >docker run imagen_prueba /etc/passwd

     La parte "/etc/passwd" es lo que se le pasarÃ¡ al comando "/bin/cat" del ENTRYPOINT, dando como resultado la ejecuciÃ³n del 
     comando dentro del contenedor: 

       >/bin/cat/ /etc/passwd

     - Otro Ejemplo:
        
	# Dockerfile
	FROM debian:wheezy
	ENTRYPOINT ["/bin/ping"]
	CMD ["localhost"]

     La creaciÃ³n de un contenedor basado en el Dockerfile anterior, darÃ­a como resultado:

      >$ docker run -it test
	PING localhost (127.0.0.1): 48 data bytes
	56 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.096 ms
	56 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.088 ms
	56 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.088 ms
	^C--- localhost ping statistics ---
	3 packets transmitted, 3 packets received, 0% packet loss
	round-trip min/avg/max/stddev = 0.088/0.091/0.096/0.000 ms     

     Pero si aÃ±adimos un "argumento", anularÃ¡ lo que estuviese indicado en CMD ["localhost"] y harÃ¡ caso al argumento que le pasemos:

      >$ docker run -it test google.com
	PING google.com (173.194.45.70): 48 data bytes
	56 bytes from 173.194.45.70: icmp_seq=0 ttl=55 time=32.583 ms
	56 bytes from 173.194.45.70: icmp_seq=2 ttl=55 time=30.327 ms
	56 bytes from 173.194.45.70: icmp_seq=4 ttl=55 time=46.379 ms
	^C--- google.com ping statistics ---
	5 packets transmitted, 3 packets received, 40% packet loss
	round-trip min/avg/max/stddev = 30.327/36.430/46.379/7.095 ms


     F-1) How CMD and ENTRYPOINT interact?
          
	  Both CMD and ENTRYPOINT instructions define what command gets executed when running a container. There are few rules 
          that describe their co-operation.
		
		1) Dockerfile should specify at least one of CMD or ENTRYPOINT commands.

		2) ENTRYPOINT should be defined when using the container as an executable.

		3) CMD should be used as a way of defining default arguments for an ENTRYPOINT command or for executing an 
		   ad-hoc command in a container.

		4) CMD will be overridden when running the container with alternative arguments.
    
      

 
