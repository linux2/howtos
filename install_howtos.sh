#!/bin/bash

# Set your shell: BASH or ZSH
shell=.bashrc

# Check current shell
cs=`echo $SHELL | awk -F '/bin/' '{print $2}'`

if [[ $cs != "bash" ]];then
    shell=".zshrc"
fi

# Change directory to $HOME
cd $HOME

echo ""
echo -e "\e[32mInstalling HowTo alias list on $shell..."
echo -e "\e[39m"

echo "
########################################
## Mis HowTos                         ##
########################################

alias mishowtos='more ~/01-HowTos/00-Mis_howtos_terminal.txt'

alias howtocompilec='c && more ~/01-HowTos/c_c++/01-C_compilation.txt'
alias howtoconsolecable='c && more ~/01-HowTos/raspberry/01-Conectar_Cable_Consola.txt'
alias howtocurl='c && more ~/01-HowTos/linux_general/05-curl.txt'
alias howtocurl='c && more ~/01-HowTos/linux_general/05-curl.txt'
alias howtodockerbasico='c && more ~/01-HowTos/docker/01-comandos_basicos.txt'
alias howtodockertipico='c && more ~/01-HowTos/docker/02-comandos_tipicos.txt'
alias howtoelkdocker='c && more ~/01-HowTos/elastic_elk/01-ELK_stack_en_Docker_x86.txt'
alias howtofind='c && more ~/01-HowTos/linux_general/06-Busquedas.txt'
alias howtogit='c && more ~/01-HowTos/git_github_gitlab/01-pasos_resumidos_git_github.txt'
alias howtogpg='c && more ~/01-HowTos/gpg/01-GPG_comandos_generales.txt'
alias howtogrepegrep='c && more ~/01-HowTos/linux_general/03-grep_egrep.txt'
alias howtoi3='c && more ~/01-HowTos/i3/01-misi3shortcuts.txt'
alias howtoiptables='c && more ~/01-HowTos/networking/01-comandos_generales_iptables.txt'
alias howtolinuxusergroups='c && more ~/01-HowTos/linux_general/01-Gestion_usuarios_grupos_Linux.txt'
alias howtomongodb='c && more ~/01-HowTos/databases/mongodb/01-comandos_utiles_mongodb.txt'
alias howtomountusb='c && more ~/01-HowTos/linux_general/07-montar_usb_drives_howto.txt'
alias howtomysql='c && more ~/01-HowTos/databases/mysql/01-comandos_utiles_mysql.txt'
alias howtomqttl='c && more ~/01-HowTos/mqtt/01-comandos_utiles_mosquitto.txt'
alias howtonetcat='c && more ~/01-HowTos/networking/06-netcat.txt'
alias howtonetstat='c && more ~/01-HowTos/networking/04-Netstat.txt'
alias howtoneovim='c && more ~/01-HowTos/linux_general/10-neovim_nvim.txt'
alias howtonmap='c && more ~/01-HowTos/networking/03-NMAP.txt'
alias howtopacman='c && more ~/01-HowTos/arch_linux/01-pacman_howto.txt'
alias howtopacmanmainten='c && more ~/01-HowTos/arch_linux/02-Arch_Maintenance.txt'
alias howtopip='c && more ~/01-HowTos/python/04-Pip_howto.txt'
alias howtopostgresql='c && more ~/01-HowTos/databases/postgresql/01-comandos_utiles_postgresql.txt'
alias howtopythonproject='c && more ~/01-HowTos/python/01-python_start_project.txt'
alias howtoregex='c && more ~/01-HowTos/regex/01-Resumen_RegEx.txt'
alias howtorouting='c && more ~/01-HowTos/networking/02-linux_routing.txt'
alias howtorpibackup='c && more ~/01-HowTos/raspberry/02-Backup_RPi_ISO.txt'
alias howtorsyncbackup='c && more ~/01-HowTos/rsync/01-Automatizar_backups_con_RSYNC.txt'
alias howtoscrapyinstall='c && more ~/01-HowTos/scrapy_web-scraping/01-Instalacion.txt'
alias howtoscrapypipemongo='c && more ~/01-HowTos/scrapy_web-scraping/03-Pipe_a_MongoDB.txt'
alias howtoscrapyproject='c && more ~/01-HowTos/scrapy_web-scraping/02-Crear_nuevo_proyecto.txt'
alias howtoscrapyshell='c && more ~/01-HowTos/scrapy_web-scraping/04-trabajar_con_scrapy_shell.txt'
alias howtoscreen='c && ~/01-HowTos/screen/01-Screen.txt'
alias howtosed='c && more ~/01-HowTos/linux_general/02-sed.txt'
alias howtoservices='c && more ~/01-HowTos/linux_general/08-Services_updaterc.txt'
alias howtossh='c && more ~/01-HowTos/ssh/01-SSH_y_GitHub.txt'
alias howtosshtunneling='c && more ~/01-HowTos/ssh/02-SSH_Tunneling.txt'
alias howtotargzip='c && more ~/01-HowTos/linux_general/04-Comprimiendo_con_tar_gzip_linea_de_comandos.txt'
alias howtotcpdump='c && more ~/01-HowTos/networking/05-tcpdump.txt'
alias howtotmux='c && more ~/01-HowTos/tmux/01-tmux_howto.txt'
alias howtovim='c && more ~/01-HowTos/vim_neovim/01-vim_comandos_basicos.txt'
alias howtovirtualenv='c && more ~/01-HowTos/python/02-VirtualEnvironments_Venv_Pipenv.txt'
alias howtovirtualenvwrapper='c && more ~/01-HowTos/python/03-howtovirtualenvwrapper.txt'
alias howtowificli='c && more ~/01-HowTos/linux_general/09-wifi_cli_archlinux.txt'
" >> $shell

# Post-installation tasks
# - Reload bashrc file
echo -e "\e[32mReloading $shell file..."
echo -e "\e[39m"
source ~/$shell

echo ""
echo -e "\e[32mInstallation finished!"
echo -e "\e[32mEnjoy it! :D"
echo -e "\e[39m"
