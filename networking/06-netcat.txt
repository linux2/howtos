####################################################################################################################################################################################
########################################################################## NETCAT: Howto ###########################################################################################
####################################################################################################################################################################################
 _______________
|		|
| INTRODUCCIÃN  |__________________________________________________________________________________________________________________________________________________________________
|_______________|


 1) Â¿QuÃ© es y para quÃ© sirve?
 ----------------------------

    Ncat is a general-purpose command-line tool for reading, writing, redirecting, and encrypting data across a network.It aims to be your network "Swiss Army knife", handling a 
    wide variety of security testing and administration tasks.


 2) Â¿QuÃ© puede hacer?
  --------------------

    Ncat can:

	A) Act as a simple TCP/UDP/SCTP/SSL client for interacting with:
		- web servers, 
		- telnet servers, 
		- mail servers, 
		- other TCP/IP network services. 
	  
	   Often the best way to understand a service (for fixing problems, finding security flaws, or testing custom commands) is to interact with it using Ncat. This lets you 
	   you control every character sent and view the raw, unfiltered responses.


	B) Act as a simple TCP/UDP/SCTP/SSL server for offering services to clients, or simply to understand what existing clients are up to by capturing every byte they send.

	C) Redirect or proxy TCP/UDP/SCTP traffic to other ports or hosts. This can be done using simple redirection (everything sent to a port is automatically relayed somewhere 
	   else you specify in advance) or by acting as a SOCKS or HTTP proxy so clients specify their own destinations. In client mode, Ncat can connect to destinations through 
	   a chain of anonymous or authenticated proxies.

	D) Run on all major operating systems. We distribute Linux, Windows, and Mac OS X binaries, and Ncat compiles on most other systems. A trusted tool must be available 
	   whenever you need it, no matter what computer you're using.

	E) Encrypt communication with SSL, and transport it over IPv4 or IPv6.

	F) Act as a network gateway for execution of system commands, with I/O redirected to the network. It was designed to work like the Unix utility cat, but for the network.

	G) Act as a connection broker, allowing two (or far more) clients to connect to each other through a third (brokering) server. This enables multiple machines hidden 
	   behind NAT gateways to communicate with each other, and also enables the simple Ncat chat mode.
  

  3) Â¿QuÃ© relaciÃ³n hay entre Netcat (nc) y Ncat?
  ----------------------------------------------	

     Ncat is our modern reinvention of the venerable Netcat (nc) tool released by Hobbit in 1996. 
     While Ncat is similar to Netcat in spirit, they don't share any source code. 
     Instead, Ncat makes use of Nmap's well optimized and tested networking libraries. 
     Compatibility with the original Netcat and some well known variants is maintained where it doesn't conflict with Ncat's enhancements or cause usability problems. 
     Ncat adds many capabilities not found in Hobbit's original nc, including:
	- SSL support, 
	- proxy connections, 
	- IPv6, 
	- connection brokering. 

     The original nc contained a simple port scanner, but we omitted that from Ncat because we have a preferred tool for that function.

 _______________
|		|
| PROTOCOLS     |__________________________________________________________________________________________________________________________________________________________________
|_______________|

  Ncat can use: TCP, UDP, SCTP, SSL, IPv4, IPv6, and various combinations of these. TCP over IPv4 is the default.

	- TCP (Transmission Control Protocol): is the reliable protocol that underlies a great deal of Internet traffic. Ncat makes TCP connections by default. TCP may be 
					       combined with SSL.

	- UDP (User Datagram Protocol): is an unreliable protocol often used by applications that can't afford the overhead of TCP. Use the "--udp" option to make Ncat use UDP. 
					In listen mode, Ncat will communicate with only one client, and the "--keep-open" option doesn't work, the reason for this being that UDP 
					has no notion of a connection. UDP may not be combined with SSL.

	- SCTP (Stream Control Transmission Protocol): is a newer reliable protocol. It is selected with the "--sctp" option. Ncat uses a TCP-compatible subset of SCTP features, 
						       not including multiple streams per connection or message boundaries. SCTP may be combined with SSL.

	- SSL (Secure Sockets Layer) or TLS (Transport Layer Security): both provides security to network traffic when used properly. Use the --ssl to turn SSL on; it works with 
									TCP or SCTP. See the section called âSSLâ for instructions and caveats.

	- IPv4 (Internet Protocol version 4): is the dominant version of the Internet Protocol in use. Ncat uses it by default. Using the "-4" puts Ncat into IPv4-only mode; only 
					      IPv4 addresses will be used even if, for example, as hostname resolves to IPv6 addresses as well.

	- IPv6: is the lesser-used successor to IPv4. Use "-6" to put Ncat into IPv6-only mode.

 _______________
|		|
| CÃMO UTILIZAR |__________________________________________________________________________________________________________________________________________________________________
|_______________|

  Ncat always operates in one of two basic modes: CONNECT MODE and LISTEN MODE.


  A) CONNECT MODE: 
     -------------

     In connect mode, Ncat initiates a connection (or sends UDP data) to a service that is listening somewhere. For those familiar with socket programming, connect mode is like 
     using the connect function. 
 
     To use Ncat in connect mode, run:

	>ncat <host> [<port>]

	  <host>   => may be a hostname or IP address.
	  [<port>] => a port number.

	     Example:
	     --------

	     In this case we use Ncat to manually retrieve a web page from an HTTP server, just as web browsers do in the background when you visit a web site. 
	     The blank line after the GET line is requiredâjust hit enter twice:

	      >ncat -C scanme.nmap.org 80

		-C: The -C option turns on "CRLF" (CRLF es la combinaciÃ³n de CR + LF,es decir, CARRIAGE RETURN + LINE FEED) replacement, which replaces any line endings you type 
		    with CRLF. CRLF line endings are required by many protocols, including HTTP, though many servers will accept a plain newline (LF) character.
	
	
  B) LISTEN MODE: 
     ------------

     In listen mode, Ncat waits for an incoming connection (or data receipt), like using the bind and listen functions. You can think of connect mode as âclientâ mode and listen 
     mode as âserverâ mode.

     Listen mode is the same, with the addition of the --listen option (or its -l alias):

	>ncat --listen [<host>] [<port>]
	>ncat -l [<host>] [<port>]
	
	  <host>   => controls the address on which Ncat listens; if you omit it, Ncat will bind to all local interfaces (INADDR_ANY).
	  [<port>] => If the port number is omitted, Ncat uses its default port 31337.

     Typically only privileged (root) users may bind to a port number lower than 1024. A listening TCP server normally accepts only one connection and will exit after the client 
     disconnects. Combined with the "--keep-open" option, Ncat accepts multiple concurrent connections up to the connection limit. With "--keep-open" (or -k for short), the 
     server receives everything sent by any of its clients, and anything the server sends is sent to all of them. A UDP server will communicate with only one client (the first 
     one to send it data), because in UDP there is no list of âconnectedâ clients.

	     Example:
	     -------

	     This time we will use Ncat as a "Web Server". Create a text file called "hello.http" with these contents:

			HTTP/1.0 200 OK

			<html>
			  <body>
			    <h1>Hello, world!</h1>
			  </body>
			</html>

	      Now we run the command:

	        >ncat -l localhost 8080 < hello.http

	      This instructs Ncat to listen on the local port 8080 and read "hello.http" on its input. Ncat is now primed to send the contents of the file as soon as it receives a 
	      connection.

	      Now open a web browser and type in the address: http://localhost:8080/


 ________________
|		 |
| USOS PRÃCTICOS |__________________________________________________________________________________________________________________________________________________________________
|________________|


  A) FILE TRANSFER:
     -------------

     IMPORTANT: Ncat by default sends all its traffic without encryption, so it is possible for someone to intercept files in transit.

     A basic file transfer is very simple: 

	A-1) Start Ncat in "Listen Mode" on one end.
	A-2) Start Ncat in "Connect Mode" on the other end, and pipe the file over the connection. 

     There are two ways to do this that differ only in which end listens, the sender or the receiver. Sometimes you can't create a listening socket on one end of the transfer 
     because of a lack or permissions, NAT, or filtering. As long as you can listen on at least one end, though, you can use this technique.

     IMPORTANT: the order of the commands. The LISTENER must be started FIRST, regardless of the direction of transfer, or else the client will not have anything to connect to.
     
	Example A: Sendinf one file
	---------

	These examples show how to transfer inputfile on "host1" to outputfile on "host2". Here no port number was specified so Ncat will use its "default port" of 31337. To use 
	a different port just list it on the command line.

	  HOST1: Sender sends "inputfile".
	  HOST2: Receiver receives in "outputfile".

		OpciÃ³n #1: Receiver (Host2) LISTENs:
	        ---------
		 
		   >host2$: ncat -l > outputfile
		   >host1$: ncat --send-only host2 < inputfile	

		OpciÃ³n #2: Sender (Host1) LISTENs:
		---------
	
		   >host1$: ncat -l --send-only < inputfile
		   >host2$: ncat host1 > outputfile

	Example B: Sendinf one file
	---------

	 Here's how to transfer <files> using the âreceiver listensâ method, though of course the âsender listensâ method works just as well.

	 Transfer a bundle of files:

		>host2$: ncat -l | tar xzv
		>host1$: tar czv <files> | ncat --send-only host2

  B) SEND MAIL:
     ---------

     It is great fun to interact with text-based network protocols with nothing more than Ncat and a keyboard. Here's a short example showing how to send email by talking to an 
     SMTP server. SMTP is described in RFC 5321, but you don't need to know much about the protocol to send a simple message. 

     The service's assigned port number is 25, and we use "-C" because it requires CRLF line endings:

	>$:ncat -C mail.example.com 25
	 220 mail.example.com ESMTP
	 HELO client.example.com
	 250 mail.example.com Hello client.example.com
	 MAIL FROM:a@example.com
	 250 OK
	 RCPT TO:b@example.com
	 250 Accepted
	 DATA		
	 354 Enter message, ending with "." on a line by itself
	 From: a@example.com
	 To: b@example.com
	 Subject: Greetings from Ncat

	 Hello. This short message is being sent by Ncat.
	 .	
	 250 OK
	 QUIT
	 221 mail.example.com closing connection


  C) CHAT:
     ----

     In its most basic form, Ncat simply moves bits from one place to another. This is all that is needed to set up a "simple chat system". By default, Ncat reads from standard 
     input and writes to standard output, meaning that it will send whatever is typed at the keyboard and will show on the screen whatever is received.

      C-1) Two-user chat:
	   --------------

	  >host1$: ncat -l
	  >host2$: ncat host1

	 IMPORTANT: Which side listens and which side connects is not important in this situation, except that the listener must start ncat first.

      C-2) Multi-user chat:
	   ---------------

	 The above technique is limited to "one-on-one" conversations. If more users connect to the server, each one will effectively create a new chat channel with the server; 
	 none of the connecting users will hear each other. Multi-user chatting is easily supported using connection brokering with the "--broker" option.

	 In "broker" mode, anything received on one connection is sent out to all other connections, so everyone can talk with everyone else. 

	 When many users are chatting through a connection broker, it can be hard to know "who is saying what". For these cases Ncat provides a simple "hack" to tell users apart. 
	 When the "--chat" option is given, connection brokering is automatically enabled. Each message received is prefixed with an ID before being relayed to all other clients. 
	 The ID is unique for each client connection, and therefore functions something like a username. Also, in chat mode any control characters are escaped so they won't mess 
	 up your terminal. The server is started with:

		>server$: ncat -l --chat

  D) SIMPLE WEB SERVER:
     -----------------

     Continuing the example from the section called âA Listen Mode Exampleâ, we can create a "simple HTTP server" that serves the "index.html" file using the following command:

	>ncat -lk -p 8080 --sh-exec "echo -e 'HTTP/1.1 200 OK\r\n'; cat index.html"

     This will start the "HTTP server", serving the "index.html" file from your current working directory. 
     To try it out, visit: http://localhost:8080/ using your web browser. You can also skip :8080 from the URL if you specified -p 80 instead of -p 8080 in the command above. 

