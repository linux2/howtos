Compilation steps of a program
==============================

The different steps to compile a program:

  A) Source Code	==> Fichero "kaka.c" donde creamos el contenido de nuestro programa.
  B) Preprocessor	==> Primer paso de la compilaciÃ³n. Tiene varias funciones principales: 1)#include   2)#define   3)Conditional_compilation   4)Macros_with_arguments 
  C) Compiler
  D) Assembler
  E) Linker
  F) Binary Executable


The Tools
=========

Besides knowing exactly what you want to achieve, you need to be familiar with the tools to achieve what you want. And there is a lot more to Linux development tools than gcc, although 
it alone would be enough to compile programs, but it would be a tedious task as the size of your project increases.

  A) MAKE:
     ----

     Definition: a file that describes the relationships and dependencies between the files of a project, with the purpose of defining what should be updated/recompiled in case one or 
		 more files in the dependency chain changes. 

     Imagine you have a multi-file project, with lots of source files, the works. Now imagine that you have to modify one file (something minor) and add some code to another source 
     file. It would be painful to rebuild all the project because of that. Here's why make was created: based on file timestamps, it detects which files need to be rebuilt in order to 
     get to the desired results (executables, object files...), named targets.




How to Compile C programs in Linux
==================================

1) Code some program and save it with ".c":

  >nano hello_world.c

    #include <stdio.h>

    int main()
      {
       printf("Hello,World!\n");
       return 0;
      }

2) Compile it with gcc compiler:

  >gcc -o <desided output name> <coded hello_world.c program>
  >gcc -o hello_world hello_world.c

3) Run the generated binary:

  >./hello_world


4) if we want to see all the intermediate files generated during A to F steps:

  >gcc -save-temps hello_world.c

  it will generate:

      a.out
      hello_world.i
      hello_world.o
      hello_world.s



